# Markdown : Syntaxe :

- [Markdown : Syntaxe :](#markdown--syntaxe-)
  - [Vue d'ensemble](#vue-densemble)
    - [Philosophie](#philosophie)
  - [Éléments de bloc](#éléments-de-bloc)
    - [Paragraphes et sauts de ligne](#paragraphes-et-sauts-de-ligne)
    - [En-têtes](#en-têtes)
    - [Citations en bloc](#citations-en-bloc)
    - [Listes](#listes)
    - [Blocs de code](#blocs-de-code)
  - [Éléments de la portée](#éléments-de-la-portée)
    - [Liens](#liens)
    - [Accent](#accent)
    - [Code](#code)


**Note:** Ce document est lui-même rédigé en utilisant la méthode de notation ; vous
peut [voir la source en ajoutant '.text' à l'URL](/projects/markdown/syntax.text).

----

## Vue d'ensemble

### Philosophie

La notation se veut aussi facile à lire et à écrire que possible.

La lisibilité est toutefois privilégiée par-dessus tout. Un format de Markdown
le document doit pouvoir être publié tel quel, en texte clair, sans qu'il soit nécessaire de regarder
comme s'il avait été marqué avec des balises ou des instructions de formatage. Alors que
La syntaxe de Markdown a été influencée par plusieurs text-to-HTML existants
y compris [Setext](http://docutils.sourceforge.net/mirror/setext.html), [atx](http://www.aaronsw.com/2002/atx/), [Textile](http://textism.com/tools/textile/), [reStructuredText](http://docutils.sourceforge.net/rst.html),
[Grutatext](http://www.triptico.com/software/grutatxt.html), et [EtText](http://ettext.taint.org/doc/) - la plus grande source de
La syntaxe de Markdown s'inspire du format du courrier électronique en texte clair.

## Éléments de bloc

### Paragraphes et sauts de ligne

Un paragraphe est simplement une ou plusieurs lignes de texte consécutives, séparées
par une ou plusieurs lignes blanches. (Une ligne blanche est toute ligne qui ressemble à une
ligne blanche -- une ligne ne contenant que des espaces ou des tabulations est considérée
blanc). Les paragraphes normaux ne doivent pas être indentés avec des espaces ou des tabulations.

L'implication de la règle "une ou plusieurs lignes de texte consécutives" est
que le Markdown supporte les paragraphes de texte "hard-wrapped". Cela diffère
de la plupart des autres formateurs de texte en HTML (y compris Movable
L'option "Convertir les sauts de ligne" de Type) qui traduit chaque saut de ligne
dans un paragraphe dans une balise `<br />`.

Lorsque vous *voulez* insérer une balise `<br />` break en utilisant Markdown, vous
terminez une ligne avec deux espaces ou plus, puis tapez retour.

### En-têtes

La démarque supporte deux styles d'en-têtes, [Setext] [1] et [atx] [2].

En option, vous pouvez "fermer" les en-têtes de type atx. Ceci est purement
cosmétique - vous pouvez l'utiliser si vous pensez qu'il est plus beau. Le site
les hachages de clôture ne doivent même pas correspondre au nombre de hachages
utilisé pour ouvrir l'en-tête. (Le nombre de hachages d'ouverture
détermine le niveau de l'en-tête).


### Citations en bloc

La démarque utilise les caractères de style ">" pour les citations en bloc. Si vous êtes
vous êtes habitué à citer des passages de texte dans un message électronique, alors
savoir comment créer une citation dans Markdown. Il est préférable que vous
enveloppez le texte et mettez un ">" avant chaque ligne :

> Il s'agit d'une citation avec deux paragraphes. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.

La démarque vous permet d'être paresseux et de ne mettre le ">" qu'avant le premier
ligne d'un paragraphe bien ficelé :

> Il s'agit d'une citation avec deux paragraphes. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.

> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
id sem consectetuer libero luctus adipiscing.

Les citations en bloc peuvent être imbriquées (c'est-à-dire une citation en bloc dans une citation en bloc) par
en ajoutant des niveaux supplémentaires de ">" :

> C'est le premier niveau de citation.
>
> > Il s'agit d'une citation imbriquée.
>
> Retour au premier niveau.

Les Blockquotes peuvent contenir d'autres éléments de Markdown, y compris des en-têtes, des listes,
et des blocs de code :

> ## Ceci est un en-tête.
> 
> 1. il s'agit du premier élément de la liste
> 2.   C'est le deuxième élément de la liste.
> 
> Voici un exemple de code :
> 
> return shell_exec("echo $input | $markdown_script") ;

Tout bon éditeur de texte devrait faciliter la citation par courriel. Pour
Par exemple, avec BBEdit, vous pouvez faire une sélection et choisir Augmenter
Niveau de citation dans le menu Texte.


### Listes

La démarque prend en charge les listes ordonnées (numérotées) et non ordonnées (à puces).

Les listes non ordonnées utilisent des astérisques, des plus et des traits d'union -- de façon interchangeable
-- comme marqueurs de liste :

* Rouge
* Vert
* Bleu

est équivalent à :

+ Rouge
+ Vert
+ Bleu

et :

- Rouge
- Vert
- Bleu

Les listes ordonnées utilisent des numéros suivis de points :

1.  Bird
2.  McHale
3.  Paroisse

vous obtiendrez exactement le même résultat en HTML. Le fait est que, si vous le souhaitez,
vous pouvez utiliser des numéros ordinaux dans vos listes de démarque ordonnée, de sorte que
les chiffres de votre source correspondent aux chiffres de votre HTML publié.
Mais si vous voulez être paresseux, vous n'êtes pas obligé de le faire.

Pour que les listes aient l'air agréables, vous pouvez envelopper les articles avec des tirets suspendus :

* Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
    Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
    viverra nec, fringilla in, laoreet vitae, risus.
* Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
    Suspendisse id sem consectetuer libero luctus adipiscing.

Mais si vous voulez être paresseux, vous n'êtes pas obligé :

* Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
viverra nec, fringilla in, laoreet vitae, risus.
* Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
Suspendisse id sem consectetuer libero luctus adipiscing.

Les éléments de la liste peuvent comporter plusieurs paragraphes. Chaque paragraphe suivant
le paragraphe d'un élément de liste doit être indenté de 4 espaces
ou un onglet :

1.  Il s'agit d'un élément de liste avec deux paragraphes. Lorem ipsum dolor
    sit amet, consectetuer adipiscing elit. Aliquam hendrerit
    mi posuere lectus.

    Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
    sit amet velit.

2.  Suspendisse id sem consectetuer libero luctus adipiscing.

Il est agréable de mettre en retrait chaque ligne de la
paragraphes, mais là encore, la démarque vous permettra d'être
paresseux :

* Il s'agit d'un élément de liste avec deux paragraphes.

    C'est le deuxième paragraphe de la liste. Vous êtes
Il suffit d'indenter la première ligne. Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.

* Un autre élément de la même liste.

Pour mettre une citation à l'intérieur d'un élément de la liste, la citation est ">".
Les délimiteurs doivent être indentés :

* Un élément de liste avec une citation :

    > Ceci est une citation
    > à l'intérieur d'un élément de la liste.

Pour placer un bloc de code à l'intérieur d'un élément de liste, le bloc de code doit
à indenter *deux fois* -- 8 espaces ou deux tabulations :

* Un élément de liste avec un bloc de code :

        <code va ici>

### Blocs de code

Des blocs de code préformatés sont utilisés pour écrire sur la programmation ou
baliser le code source. Plutôt que de former des paragraphes normaux, les lignes
d'un bloc de code sont interprétés littéralement. La démarque enveloppe un bloc de code
dans les balises `<pre>` et `<code>`.

Pour produire un bloc de code dans Markdown, il suffit d'indenter chaque ligne de la balise
bloquer par au moins 4 espaces ou 1 tabulation.

Il s'agit d'un paragraphe normal :

    C'est un bloc de code.

Voici un exemple d'AppleScript :

    dire à l'application "Foo"
        Bip
    fin de raconter

Un bloc de code se poursuit jusqu'à ce qu'il atteigne une ligne non indentée
(ou la fin de l'article).

Dans un bloc de code, les esperluettes (`&`) et les crochets d'angle (`<` et `>`)
sont automatiquement convertis en entités HTML. Cela rend très
facile d'inclure un exemple de code source HTML à l'aide de Markdown -- il suffit de le coller
et de l'indenter, et Markdown s'occupera de l'encodage du
les esperluettes et les équerres. Par exemple, ceci :

    <div class="footer">
        &copy ; 2004 Foo Corporation
    </div>

La syntaxe de la démarque ordinaire n'est pas traitée dans les blocs de code. Par exemple,
les astérisques ne sont que des astérisques littéraux à l'intérieur d'un bloc de code. Cela signifie que
il est également facile d'utiliser Markdown pour écrire sur sa propre syntaxe.

```
dire à la demande "Foo".
    Bip
fin de raconter
```

## Éléments de la portée

### Liens

La démarque supporte deux styles de liens : *en ligne* et *référence*.

Dans les deux styles, le texte du lien est délimité par des [crochets].

Pour créer un lien en ligne, utilisez immédiatement un ensemble de parenthèses régulières
après le crochet fermant le texte du lien. A l'intérieur des parenthèses,
mettre l'URL à l'endroit où vous voulez que le lien pointe, avec un *facultatif*.
titre du lien, entouré de guillemets. Par exemple :

Voici [un exemple](http://example.com/) de lien en ligne.

[Ce lien](http://example.net/) n'a pas d'attribut de titre.

### Accent

La démarque traite les astérisques (`*`) et les soulignements (`_`) comme des indicateurs de
l'accent. Le texte entouré d'un " * " ou d'un " _ " sera entouré d'un
Balise HTML `<em>` ; les doubles `*` ou `_` seront enveloppés d'un HTML
tag `<strong>`. Par exemple, cette entrée :

*astérisques simples*

_single underscores_ (soulignement simple)

**double astérisque**

__double soulignement

### Code

Pour indiquer une plage de code, il faut l'entourer de guillemets ("````).
Contrairement à un bloc de code pré-formaté, un span de code indique le code à l'intérieur d'un
paragraphe normal. Par exemple :

Utilisez la fonction `printf()`.
