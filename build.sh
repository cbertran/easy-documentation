#!/bin/bash
ARGS_PATH="srcs/js"
ARGS=( checkService.js configuration.js hash.js markdown.js url.js highlight.min.js markdownit.min.js markdownit-option.min.js markdownit-emoji.min.js EasyDoc.js )
EasyDoc_comment="/**
 * 
 * EasyDoc.min.js contain full compressed code of projet
 *
 * @summary	EasyDoc.min.js
 * @author	Clément Bertrand
 * @license	LGPLv3
 * @since	2021
 * 
 */"

mkdir out
touch EasyDoc.min.js
echo "$EasyDoc_comment" > EasyDoc.min.js
for id in ${ARGS[@]}
do
	cat $ARGS_PATH/$id >> EasyDoc.min.js
done
mv EasyDoc.min.js $ARGS_PATH/EasyDoc.min.js

ARGS_PATH="srcs/css"
ARGS=( milligram.css highlight.css EasyDoc.css )
EasyDoc_comment="/**
 * 
 * EasyDoc.min.css contain full compressed css of projet
 *
 * @summary	EasyDoc.min.css
 * @author	Clément Bertrand
 * @license	LGPLv3
 * @since	2021
 * 
 */"

mkdir out
touch EasyDoc.min.css
echo "$EasyDoc_comment" > EasyDoc.min.css
for id in ${ARGS[@]}
do
	cat $ARGS_PATH/$id >> EasyDoc.min.css
done
mv EasyDoc.min.css $ARGS_PATH/EasyDoc.min.css
