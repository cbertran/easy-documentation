class EasyDoc
{
	//#region Create instance
	constructor(configPath)
	{
		'use strict';
		if (EasyDoc.exists)
			return (EasyDoc.instance);
		this._configPath = configPath;
		this._CacheActive = window.EasyDoc_IsCacheActive;
			delete window.EasyDoc_IsCacheActive;
		this._filesHash = {};
		this._config = {};
		this._menu = {};
		this._loader = null;
		this._url = new EasyDoc_Url();
		if (!localStorage.getItem("EasyDoc._filesHash"))
			this._filesHash = Object.assign({}, {config: null, menu: null});
		else
			this._filesHash = Object.assign({}, JSON.parse(localStorage.getItem("EasyDoc._filesHash")));
		let fetch = async() => {
			await this._configFile();
			await this._menuFile();
			localStorage.setItem("EasyDoc._filesHash", JSON.stringify(this._filesHash));
			this._url.rewrite();
			this.AddActiveLinkList(document.getElementById('EasyDoc_List').getElementsByTagName('dt'));
			EasyDoc_markdown.open(this._url._parse()[2] + ".md");
		}; fetch();
		EasyDoc.instance = this;
		EasyDoc.exists = true;
	}

	async _configFile()
	{
		'use strict';
		const config 		= await this._fetch(this._configPath);
		const hashConfig	= await Hash.checksum(config).then(hash => {
								return (hash);
							});
		if (hashConfig && this._filesHash.config !== hashConfig)
		{
			this._filesHash.config = hashConfig;
			this._config = Object.assign({}, JSON.parse(config));
			localStorage.setItem("EasyDoc._language", this._config.lang.default);
			localStorage.setItem("EasyDoc._config", config);
		}
		else
			this._config = Object.assign({}, JSON.parse(localStorage.getItem("EasyDoc._config")));
	}

	async _menuFile(updateOnlyMenu)
	{
		'use strict';
		let _configuration;
		let lang = String(this._url._parse()[0]);
		if (lang)
			this.checkLang(lang);
		let linkMenu = "./" + this._config.directory.document + "/" + localStorage.getItem("EasyDoc._language") + "/menu.json";
		const menu 			= await this._fetch(linkMenu);
		const hashMenu		= await Hash.checksum(menu).then(hash => {
								return (hash);
							});
		if (hashMenu && this._filesHash.menu !== hashMenu)
		{
			this._filesHash.menu = hashMenu;
			this._menu = Object.assign({}, JSON.parse(menu));
			localStorage.setItem("EasyDoc._menu", menu);
			_configuration = new EasyDoc_configuration();
			localStorage.setItem("EasyDoc._genConfig", JSON.stringify(_configuration.getConfig()));
		}
		else
		{
			this._menu = Object.assign({}, JSON.parse(localStorage.getItem("EasyDoc._menu")));
			if (typeof updateOnlyMenu !== 'undefined')
				_configuration = new EasyDoc_configuration(JSON.parse(localStorage.getItem("EasyDoc._genConfig"), true));
			else
				_configuration = new EasyDoc_configuration(JSON.parse(localStorage.getItem("EasyDoc._genConfig"), false));
		}
		if (typeof updateOnlyMenu !== 'undefined')
			_configuration._apply(true);
		else
			_configuration._apply();
	}

	async _fetch(link)
	{
		'use strict';
		return fetch(link).then(response => {
			if (!response.ok)
				return ('{}');	
			return (response.text());
		});
	}

	checkLang(lang)
	{
		'use strict';
		let ifFoundLang = false;
		for (let id in window.EasyDoc._config.lang)
		{
			if (lang === id)
			{
				ifFoundLang = true;
				break ;
			}
		}
		if (ifFoundLang === false)
			lang = window.EasyDoc._config.lang.default;
		localStorage.setItem("EasyDoc._language", lang);
	}
	
	//#endregion Create instance

	AddActiveLinkList(list)
	{
		'use strict';
		for (let element of list)
		{
			if (element.id === localStorage.getItem("EasyDoc._id"))
				element.classList.add("activeLink");
		}
	}
}

function launchEasyDoc(configPath)
{
	'use strict';
	if (!CheckServicesAvailable.fetch())
	{
		document.body.innerHTML = "This site will not work on your browser, fetch request is not implemented. Update your browser.";
		return (null);
	}
	if (!CheckServicesAvailable.cacheStorage())
		console.log("Cache service is not present in your browser, files will not be cached correctly.");
	if (CheckServicesAvailable.localStorage())
	{
		window.EasyDoc = new EasyDoc(configPath);
		return (window.EasyDoc);
	}
	else
	{
		document.body.innerHTML = "This site will not work on your browser, localstorage is not working.";
		return (null);
	}
}

document.getElementById('EasyDoc_Lang').addEventListener('input', (event) => {
	'use strict';
	let fetch = async() => {
		localStorage.setItem("EasyDoc._language", event.target.value);
		window.EasyDoc._url.rewrite(event.target.value, undefined);
		await window.EasyDoc._menuFile(true);
		window.EasyDoc.AddActiveLinkList(document.querySelectorAll('#EasyDoc_List dt'));
		EasyDoc_markdown.open(window.EasyDoc._url._parse()[2] + ".md");
	}; fetch();

}, false);

document.getElementById('EasyDoc_List').addEventListener('click', (event) => {
	'use strict';
	if (event.target.tagName == 'SPAN')
	{
		if (!event.target.classList.contains('arrow-animation-inverse'))
			event.target.innerText = '▲';
		else
			event.target.innerText = '▼';
		event.target.classList.toggle('arrow-animation-inverse');
		event.target.parentNode.nextSibling.classList.toggle('dlHide');
		return ;
	}
	else if (event.target.tagName !== 'DT')
		return ;
	const _reset = {
		dt: document.querySelectorAll('#EasyDoc_List dt'),
		arrow: document.querySelectorAll('#EasyDoc_List span')
	};
	for (let element of _reset.dt)
		element.classList.remove("activeLink");
	for (let element of _reset.arrow)
		element.classList.remove("activeLink");
	event.target.classList.add("activeLink");
	localStorage.setItem("EasyDoc._id", event.target.id);
	window.EasyDoc._url.rewrite(undefined, event.target.id);
	EasyDoc_markdown.open(event.target.dataset.link);
}, false);


document.getElementById('loader').addEventListener('transitionend', (event) => {
	'use strict';
	event.target.style.display = "none";
}, false);

document.getElementById('toTheTop').addEventListener('click', (event) => {
	'use strict';
	event.preventDefault();
	event.stopImmediatePropagation();
	document.getElementById('EasyDoc_document').scrollIntoView(
		{
			behavior:"smooth",
			block:"start",
			inline:"nearest"
		}
	);
});
