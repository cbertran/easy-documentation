const image_modal = document.getElementsByClassName('EasyDoc_modal')[0];
document.getElementsByClassName('EasyDoc_modal_close')[0].addEventListener('click', () => {
	image_modal.style.display = 'none';
});
document.getElementsByClassName('EasyDoc_modal')[0].addEventListener('click', (event) => {
	if (event.target.classList.contains('EasyDoc_modal'))
		image_modal.style.display = 'none';
});

class EasyDoc_markdown
{
	static open(input)
	{
		'use strict';
		this.read(window.EasyDoc._url._href + window.EasyDoc._config.directory.document + "/" + localStorage.getItem("EasyDoc._language") + "/" + input);
	}
	
	static read(link)
	{
		'use strict';
		document.getElementById("EasyDoc_document").style.setProperty("opacity", "0");
		document.getElementById("loader").style.setProperty("display", "flex");
		document.getElementById("loader").classList.replace("hidden", "visible");
		fetch(link).then(response => {
			if (!response.ok)
				throw Error(response.statusText);
			return (response.text());
		}).then(data => {
			hljs.initHighlighting();
			let converter = window.markdownit({
				html: true,
				typographer: true,
				linkify: true,
				highlight : function (str, lang) {
					if (lang && hljs.getLanguage(lang)) {
						try {
							return hljs.highlight(lang, str, true).value;
						} catch (__) {}
					}
					return '';
				}
			})
			.use(window.markdownitEmoji)
			.use(window.markdownitSub)
			.use(window.markdownitSup)
			.use(window.markdownitIns)
			.use(window.markdownitMark)
			.use(window.markdownitFootnote)
			.use(window.markdownitDeflist)
			.use(window.markdownitAbbr)
			.use(window.markdownItAnchor);
			document.getElementById("EasyDoc_document").innerHTML = converter.render(data);
			document.getElementById("EasyDoc_document").scrollIntoView(
				{
					behavior:"smooth",
					block:"start",
					inline:"nearest"
				}
			);
			document.getElementById("loader").classList.replace("visible", "hidden");
			document.getElementById("EasyDoc_document").style.setProperty("opacity", "1");
			document.querySelector('article').querySelectorAll('img').forEach((image) => {
				image.addEventListener('click', () => {
					image_modal.style.display = 'flex';
					document.getElementsByClassName('EasyDoc_modal_image')[0].src = image.src;
				});
			});
		}).catch(() => {
			this.read(window.EasyDoc._url._href + window.EasyDoc._config.directory.document + "/" + localStorage.getItem("EasyDoc._language") + "/404.md");
		});
	}
}
