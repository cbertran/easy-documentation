class CheckServicesAvailable
{
	static fetch()
	{
		'use strict';
		if (window.fetch)
			return (true);
		return (false);
	}
	
	static localStorage()
	{
		'use strict';
		try {
			let x = "__storage_test__";
			localStorage.setItem(x, x);
			localStorage.removeItem(x);
			return (true);
		}
		catch(e) {
			return (e instanceof DOMException && (e.code === 22 || e.code === 1014 || e.name === 'QuotaExceededError' || e.name === 'NS_ERROR_DOM_QUOTA_REACHED') && (storage && storage.length !== 0));
		}
	}

	static cacheStorage()
	{
		'use strict';
		let isCacheActive = 'caches' in window;
		if (isCacheActive)
			window.EasyDoc_IsCacheActive = true;
		else
			window.EasyDoc_IsCacheActive = false;
		return (window.EasyDoc_IsCacheActive);
	}
}
