class Hash
{
	static async sha1(data)
	{
		'use strict';
		const msgBuffer = new TextEncoder('utf-8').encode(data);
		const hashBuffer = await crypto.subtle.digest('SHA-1', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return (hashHex);
	}

	static async sha256(data)
	{
		'use strict';
		const msgBuffer = new TextEncoder('utf-8').encode(data);
		const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return (hashHex);
	}

	static async sha384(data)
	{
		'use strict';
		const msgBuffer = new TextEncoder('utf-8').encode(data);
		const hashBuffer = await crypto.subtle.digest('SHA-384', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return (hashHex);
	}

	static async sha512(data)
	{
		'use strict';
		const msgBuffer = new TextEncoder('utf-8').encode(data);
		const hashBuffer = await crypto.subtle.digest('SHA-512', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return (hashHex);
	}

	static checksum(data)
	{
		'use strict';
		return (this.sha256(data));
	}

}
