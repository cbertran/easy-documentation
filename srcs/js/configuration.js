class EasyDoc_configuration
{
	constructor(config, updateOnlyMenu)
	{
		'use strict';
		this.isNoID = 0;
		if (typeof config !== 'undefined')
		{
			this._generate = config;
			if (typeof updateOnlyMenu !== 'undefined')
				this._generate.menu = this._menu(window.EasyDoc._menu).outerHTML;
		}
		else
		{
			this._generate = {
				title: null,
				favicon: null,
				loader: {
					css: null,
					html: null
				},
				logo: null,
				lang: {
					count: 0,
					html: null
				},
				menu: null
			};
			for (let id in window.EasyDoc._config)
			{
				if (id === 'title_page')
					this._titlePage(window.EasyDoc._config, id);
				else if (id === 'favicon')
					this._favicon(window.EasyDoc._config, id);
				else if (id === 'loader')
					this._loader(window.EasyDoc._config, id);
				else if (id === 'logo')
					this._logo(window.EasyDoc._config, id);
				else if (id === 'lang')
					this._lang(window.EasyDoc._config[id]);
			}
			for (const id in window.EasyDoc._menu)
			{
				if (window.EasyDoc._menu[id].length === 2)
					this.isNoID = 1;
				break;
			}
			this._generate.menu = this._menu(window.EasyDoc._menu).outerHTML;
		}
	}

	getConfig()
	{
		'use strict';
		return (this._generate);
	}

	_apply(updateOnlyMenu)
	{
		'use strict';
		if (typeof updateOnlyMenu === 'undefined')
		{
			if (this._generate.title)
				document.title = this._generate.title;
			if (this._generate.favicon)
			{
				let fav = document.createRange().createContextualFragment(this._generate.favicon);
				document.getElementsByTagName('head').item(0).appendChild(fav.firstChild);
			}
			if (this._generate.loader.css)
			{
				let css = document.createRange().createContextualFragment(this._generate.loader.css);
				document.getElementsByTagName('footer').item(0).appendChild(css.firstChild);
				let fetch = async() => {
					document.getElementById("loader").innerHTML = await this._fetch(this._generate.loader.html);
				}; fetch();
			}
			if (this._generate.logo)
			{
				let logo = document.createRange().createContextualFragment(this._generate.logo);
				document.getElementById("EasyDoc_List_Img").appendChild(logo.firstChild);
			}
			if (this._generate.lang.html)
			{
				if (this._generate.lang.count > 1)
				{
					let lang = document.createRange().createContextualFragment(this._generate.lang.html);
					document.getElementById("EasyDoc_Lang").appendChild(lang);
				}
				else if (document.getElementById("EasyDoc_Lang"))
					document.getElementById("EasyDoc_Lang").remove();
			}
		}
		if (this._generate.menu)
		{
			let menu = document.createRange().createContextualFragment(this._generate.menu);
			this._removeChildNodes(document.getElementById("EasyDoc_List"));
			document.getElementById("EasyDoc_List").appendChild(menu);
			document.getElementsByName('pageList').forEach((element) => {
				if (element.dataset.id === localStorage.getItem("EasyDoc._id"))
					element.classList.add("activeLink");
			});
		}
	}
	
	_removeChildNodes(parent)
	{
		'use strict';
		while (parent.firstChild)
			parent.removeChild(parent.firstChild);
	}

	async _fetch(link)
	{
		'use strict';
		return fetch(link).then(response => {
			if (!response.ok)
				throw Error(response.statusText);
			return (response.text());
		}).catch(error => {
			throw new Error(error);
		});
	}

	_titlePage(json, id)
	{
		'use strict';
		this._generate.title = json[id];
	}

	_favicon(json, id)
	{
		'use strict';
		let favicon = document.createElement('link');
		favicon.rel = 'icon';
		favicon.type = 'image/' + json[id].split('.').pop();
		favicon.href = json[id];
		this._generate.favicon = favicon.outerHTML;
	}

	_loader(json, id)
	{
		'use strict';
		let css = document.createElement('link');
		css.setAttribute('rel', 'stylesheet');
		css.setAttribute('href', json[id] + ".css");
		this._generate.loader.css = css.outerHTML;
		this._generate.loader.html = json[id] + ".html";
	}

	_logo(json, id)
	{
		'use strict';
		let logo = document.createElement('img');
		logo.setAttribute('alt', 'menu_logo');
		logo.setAttribute('src', json[id]);
		this._generate.logo = logo.outerHTML;
	}

	_lang(json)
	{
		'use strict';
		let div = document.createElement('div');
		for (let id in json)
		{
			if (id !== 'default')
			{
				let element = document.createElement('option');
				element.setAttribute('value', id);
				element.innerText = json[id];
				div.appendChild(element);
				this._generate.lang.count++;
			}
		}
		this._generate.lang.html = div.innerHTML;
	}

	_menu(json)
	{
		'use strict';
		let DL = document.createElement('dl');
		DL.classList.add('innerDl');
		let DT;
		for (let id in json)
		{
			if (Object.prototype.toString.call(json[id]) === '[object Object]')
			{
				let arrow = document.createElement('span');
				arrow.classList.add('arrow-animation');
				arrow.appendChild(document.createTextNode('▼'));
				DT.appendChild(arrow);
				DL.appendChild(this._menu(json[id]));
			}
			else
			{
				DT = document.createElement('dt');
				DT.innerText = json[id][0];
				DT.setAttribute('data-link', json[id][1]);
				if (this.isNoID > 0)
					DT.setAttribute('id', this.isNoID++);
				else
					DT.setAttribute('id', json[id][2]);
				DL.appendChild(DT);
			}
		}
		return (DL);
	}
}
