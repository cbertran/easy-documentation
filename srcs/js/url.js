class EasyDoc_Url
{
	/*
	** URL : ?[_language]&[_id]&[_page]
	*/
	
	constructor()
	{
		'use strict';
		this._page_url = new URL(document.location);
		this._href = this._page_url.origin + this._page_url.pathname;
	}

	rewrite(defineLang, defineId)
	{
		'use strict';
		let parse_args = this._parse();
		let args = {
			lang: String(parse_args[0]),
			id: parseInt(parse_args[1], 10),
			file: String(parse_args[2])
		};
		if (typeof defineLang !== 'undefined')
			args.lang = defineLang;
		if (typeof defineId !== 'undefined')
			args.id = parseInt(defineId, 10);
		this._check(args);
		this._change(args);
	}

	_parse() 
	{
		'use strict';
		this._page_url = new URL(document.location);
		return (this._page_url.search.substring(1).split('&'));
	}

	_check(args)
	{
		'use strict';
		let ifFoundLang = false;
		if (args.length > 3 || args.length < 3)
		{
			args.lang = window.EasyDoc._config.lang.default;
			args.id = window.EasyDoc._config.main_home[1];
			args.file = window.EasyDoc._config.main_home[0].replace(".md", "");
			return ;
		}
	// Lang
		for (let id in window.EasyDoc._config.lang)
		{
			if (args.lang === id)
			{
				ifFoundLang = true;
				break ;
			}
		}
		// If incorrect lang
		if (ifFoundLang === false)
			args.lang = window.EasyDoc._config.lang.default;
		localStorage.setItem("EasyDoc._language", args.lang);
	// ID
		// If not ID
		if (args.id !== parseInt(args.id, 10))
		{
			args.id = window.EasyDoc._config.main_home[1];
			args.file = window.EasyDoc._config.main_home[0].replace(".md", "");
			return ;
		}
		// Get file connected with ID
		window.EasyDoc._parseid = null;
		window.EasyDoc._parsefile = null;
		this._parseMenu(args.id);
		if (!window.EasyDoc._parseid)
		{
			args.id = window.EasyDoc._config.main_home[1];
			args.file = window.EasyDoc._config.main_home[0].replace(".md", "");
		}
		else
		{
			args.id = window.EasyDoc._parseid;
			args.file = window.EasyDoc._parsefile.replace(".md", "");
		}
		localStorage.setItem("EasyDoc._id", args.id);
		delete window.EasyDoc._parseid;
		delete window.EasyDoc._parsefile;
	}

	_parseMenu(arg)
	{
		'use strict';
		const DTS = document.querySelectorAll('#EasyDoc_List dt');
		for (const DT of DTS)
		{
			if (parseInt(DT.getAttribute('id'), 10) === arg)
			{
				window.EasyDoc._parsefile = DT.getAttribute('data-link');
				window.EasyDoc._parseid = parseInt(DT.getAttribute('id'), 10);
				return ;
			}
		}
	}

	_change(args)
	{
		'use strict';
		let nextState = { additionalInformation: args.file };
		let nextTitle = window.EasyDoc.websiteName + " | " + args.file;
		let nextURL = this._href + "?" + args.lang + "&" + args.id + "&" + args.file;
		localStorage.setItem("EasyDoc._id", args.id);
		window.history.replaceState(nextState, nextTitle, nextURL);
	}
}
